@extends('master')

@section('title', 'Inicio')

@section('content')

    {{--Barra de busqueda--}}
    <div class="col-md-5 search">
        {!! Form::open(['url' => '/search']) !!}
        <div class="input-group ">
            <i class="fas fa-search"></i>
            {!! Form::text('search_query', null, ['class' => 'form-control', 'placeholder' => '¿Buscas algo?', 'required']) !!}
            <button class="btn search_button" type="submit" id="button-addon2">Buscar</button>
        </div>
        {!! Form::close() !!}
    </div>
    {{--Fin de barra de busqueda--}}

    <section>
        {{--Slider--}}
        @include('components/sliders_home')
        {{--Fin slider--}}
    </section>

    <section>
        <div class="rows">
            <div class="col-md-12 susb shadow">
                <div class="container">
                    <p class="text">ÚNETE AL CLUB Y CONSIGUE UN 15% DE DESCUENTO</p>
                    <a href="#" class="btn btn-outline-info tetx-dark">SUSCRIBIRSE</a>
                    <hr>
                </div>
            </div>
        </div>
    </section>

    <section>
        {{--Sección de productos--}}
        <div class="products_list" id="products_list"></div>
 
        <div class="load_products">
            <a href="#" id="load_more_products">Cargar mas productos</a>
        </div>
        {{--fin Sección de productos--}}
    </section>

@endsection