@extends('master')

@section('title', $product->name)

@section('content')
<!-- Producto individual  - Inicio !-->
    <div class="product_single">
        <div class="inside">
            <div class="container">
                <div class="row">
                    <!-- Imagen de producto y galeria de ese producto !-->
                    <div class="col-md-4 pleft0">
                        <div class="slick-slider">
                            <div>
                                <a href="{{ url('/upload/'.$product->file_path.'/t_'.$product->image) }}" data-fancybox="gallery">
                                    <img src="{{ url('/upload/'.$product->file_path.'/t_'.$product->image) }}" class="img-fluid">
                                </a>
                            </div>
                            @if(count($product->getGallery) > 0)
                                @foreach($product->getGallery as $gallery)
                                <div>
                                    <a href="{{ url('/upload/'.$gallery->file_path.'/t_'.$gallery->file_name) }}" data-fancybox="gallery">
                                        <img src="{{ url('/upload/'.$gallery->file_path.'/t_'.$gallery->file_name) }}" class="img-fluid">
                                    </a>
                                </div>
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="col-md-8">
                        <h2 class="title">{{ $product->name }}</h2>
                        <div class="category">
                            <ul>
                                <li><a href="{{ url('/') }}"><i class="fas fa-house-user"></i> Inicio</a></li>
                                <li><span class="next"><i class="fas fa-chevron-right"></i></span></li>
                                <li><a href="{{ url('/store') }}"><i class="fas fa-store"></i> Tienda</a></li>
                                <li><span class="next"><i class="fas fa-chevron-right"></i></span></li>
                                <li><a href="{{ url('/store') }}"><i class="fas fa-store"></i> {{ $product->cat->name }}</a></li>
                                
                            </ul>
                        </div>

                        <div class="add_cart mtop16">
                            <div class="row">
                            {!! Form::open(['url' => '/cart/add']) !!}
                                <div class="col-md-12">
                                    <span class="price">$ {{ Config::get('') }} {{ number_format($product->price, 2, '.',',') }}</span>
                                </div>
                            </div>

                                {{-- Input minus - plus --}}
                                <h4 class="title">¿Qué cantidad deseas comprar?</h4>
                                <div class="row mtop16">
                                    <div class="col-md-1">
                                        <a href="#" class="amount_action" data-action="minus">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        {{ Form::number('quantity', 1, ['class' => 'form-control', 'min' => '1', 'id' => 'add_to_cart_cuantity']) }}
                                    </div>
                                    <div class="col-md-1">
                                        <a href="#" class="amount_action" data-action="plus">
                                            <i class="fas fa-plus"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        {{ Form::submit('Agregar al carrito', ['class' => 'btn btn-success']) }}
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        {{-- Description product --}}
                        <div class="content">
                            {!! html_entity_decode($product->content) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

