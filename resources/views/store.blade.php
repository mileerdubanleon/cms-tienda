@extends('master')

@section('title', 'Tienda')

@section('content')

    <div class="row mtop32">
        <div class="col-md-3">
            <div class="categories_list">
                <h2 class="tittle"><i class="fas fa-stream"></i> Categorias</h2>
                <ul class="categories_list">
                    @foreach($categories as $category)
                    <li class="list"><a class="link" href="{{ url('/store/category/'.$category->id.'/'.$category->slug) }}">{{ $category->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="col-md-9">
            <div class="store_white">
                <section>
                    <h2 class="store_title mtop32"> Últimos productos agregados</h2>
                        <div class="products_list" id="products_list"></div>
                            <div class="load_more_products">
                                <a href="#" class="link_more" id="load_more_products"> Cargar mas productos</a>
                            </div>
                </section>
            </div>
        </div>
    </div>

@endsection