@extends('master')

@section('title', 'Tienda - '.$category->name)

@section('custom_meta')
    <meta name="category_id" content="{{ $category->id }}">
@stop

@section('content')

    <div class="row mtop16">
        <div class="col-md-3">
            <div class="categories_list">
                <h2 class="tittle"><i class="fas fa-stream"></i> {{ $category->name }}</h2>
                <ul class="categories_list">
                    @if($category->parent != "0")
                        <li class="list">
                           <small> <a class="link text-dark"  href="{{ url('/store/category/'.$category->getParent->id.'/'.$category->getparent->slug) }}">← Regresar a <em>{{ $category->getParent->name }} </em></a></small>
                        </li>
                    @endif
                    @if($category->parent != "0")
                        <li class="list">
                            <small><a class="link text-dark"  href="{{ url('/store/')}}"> ← Regresar a la Tienda</a></small>
                        </li>
                    @endif
                    @foreach($categories as $cat)
                        <li class="list">
                            <a class="link" href="{{ url('/store/category/'.$cat->id.'/'.$cat->slug) }}">{{ $cat->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="col-md-9">
            <div class="store_white">
                <section>
                    <h2 class="store_title"> {{ $category->name }}</h2>
                        <div class="products_list" id="products_list"></div>
                            <div class="load_more_products">
                                <a class="link_more" href="#" id="load_more_products"> Cargar mas productos</a>
                            </div>
                </section>
            </div>
        </div>
    </div>

@endsection