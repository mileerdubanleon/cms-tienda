@extends('master')

@section('title', 'Busqueda')

@section('content')

    <div class="row mtop32">
        <div class="col-md-3">
            <div class="categories_list">
                <h2 class="tittle"><i class="fas fa-stream"></i> Categorias</h2>
                
            </div>
        </div>

        <div class="col-md-9">

            {{--Barra de busqueda--}}
            <div class="col-md-6 search">
                {!! Form::open(['url' => '/search']) !!}
                <div class="input-group">
                    <i class="fas fa-search"></i>
                    {!! Form::text('search_query', null, ['class' => 'form-control', 'placeholder' => '¿Buscas algo?', 'required']) !!}
                    <button class="btn search_button" type="submit" id="button-addon2">Buscar</button>
                </div>
                {!! Form::close() !!}
            </div>
            {{--Fin de barra de busqueda--}}

            <div class="store_white">
                <section>
                    <h2 class="store_title mtop32"> Buscando: {{ $query }} </h2>
                        <div class="products_list" id="products_list">
                            @foreach($products as $product)
                                <div class="product">
                                    <div class="image">
                                        <div class="overlay">
                                            <div class="btns">
                                                <a href="{{ url('/product/'.$product->id.'/'.$product->slug) }}">
                                                    <i class="far fa-eye"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fas fa-shopping-cart"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fas fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <img src="{{ url('/upload/'.$product->file_path.'/t_'.$product->image) }}" class="img-thumbnail">
                                    </div>

                                    <a href="{{ url('/product/'.$product->id.'/'.$product->slug) }}">
                                        <div class="title">{{ $product->name }}</div>
                                        <div class="price">{{ $product->price }}</div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                </section>
            </div>
        </div>
    </div>

@endsection